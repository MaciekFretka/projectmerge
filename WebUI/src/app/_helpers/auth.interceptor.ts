import {HTTP_INTERCEPTORS} from '@angular/common/http';
import {Injectable,Injector} from '@angular/core';
import {HttpInterceptor,HttpHandler,HttpRequest} from '@angular/common/http';

import {TokenStorageService} from '../services/token-storage.service';

const TOKEN_HEADER_KEY='Authorization';

@Injectable()
export class AuthInterceptor implements HttpInterceptor{
    //constructor (private token: TokenStorageService){}
    constructor (private injector: Injector){}

    // intercept(req: HttpRequest<any>,next: HttpHandler){
    //     let authReq=req;
    //     const token = this.token.getToken();
    //     if(token != null){
    //         authReq=req.clone({headers: req.headers.set(TOKEN_HEADER_KEY,'Bearer'+ token)});
    //     }
    //     return next.handle(authReq);
    // }
    intercept(req,next){
    let tokenService= this.injector.get(TokenStorageService)
    let authReq=req.clone({
        setHeaders:{
            Authorization:`Bearer ${tokenService.getToken()}`
        }
    })
    return next.handle(authReq)
    }
}
export const authInterceptorProviders = [{
    provide: HTTP_INTERCEPTORS,uceClass: AuthInterceptor,multi: true
}];