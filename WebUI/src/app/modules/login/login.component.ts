import { Component, OnInit, ViewChild } from '@angular/core';
import { NgForm } from '@angular/forms';
import { Route } from '@angular/router';
import { User } from 'src/app/models/user';
import { LoginService } from 'src/app/services/login.service';
<<<<<<< HEAD
=======
import { TokenStorageService } from 'src/app/services/token-storage.service';
>>>>>>> login

@Component({
  selector: 'app-login-component',
  templateUrl: './login.component.html',
  styles: [`
    label { font-size: 14px; height: 14px; margin-left: 6px;}
    .all-screen { width: 100%; height: 100%; }
  `]
})
export class LoginComponent implements OnInit{
  public user: User;
<<<<<<< HEAD

  constructor(private loginService: LoginService){
    this.user = {id: '', firstName: '', lastName: '', role: '', email: '', password: ''};
  }

  onSubmit() {
    this.loginService.login(this.user);
  }

  ngOnInit() {
=======
  isLoggedin=false;
  isLoginFailed=false;
  errorMessage='';
  roles:String[]=[];

  constructor(private loginService: LoginService,private tokenStorage: TokenStorageService){
    this.user = {id: '', firstName: '', lastName: '', role: '', email: '', password: '',accessToken: ''};
  }

  onSubmit() {
    
    this.loginService.login(this.user).subscribe(
      data =>{
        this.tokenStorage.saveToken(data.accessToken);
        this.tokenStorage.saveUser(data);

        this.isLoginFailed=false;
        this.isLoggedin=true;
        this.roles=this.tokenStorage.getUser().roles;
        window.location.reload;
      },
      err=>{
        this.errorMessage=err.error.message;
        this.isLoginFailed=true;
      }
    );

  }

  ngOnInit() {
    if (this.tokenStorage.getToken()) {
      this.isLoggedin = true;
      this.roles = this.tokenStorage.getUser().roles;
    }

>>>>>>> login
    if (! this.loginService.logged()){
      // TODO redirect to home
      return;
    }
<<<<<<< HEAD
=======


>>>>>>> login
  }
}
