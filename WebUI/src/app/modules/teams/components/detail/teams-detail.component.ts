import { Component, OnInit, ViewChild, Input, Output, EventEmitter } from '@angular/core';
import { NgForm } from '@angular/forms';
import { Route } from '@angular/router';
import { Team } from 'src/app/models/team';
import { CreateTeamService } from 'src/app/services/team.service';
import { AllUsersService } from 'src/app/services/all-users.service';
import { User } from 'src/app/models/user';
import { Observable } from 'rxjs';

@Component({
  selector: 'app-teams-details-component',
  templateUrl: './teams-detail.component.html',
  styles: [`
    label { font-size: 14px; height: 14px; margin-left: 6px;}
    .all-screen { width: 100%; height: 100%; }
    .state-container-dropdown{
      position: absolute;
      z-index: 1;
      width: 87.5%;
      max-height: 300px;
      background: #f6f6f6;
      overflow-y: auto;
    }
    .user{
      padding: 5px;
      color: #A550BC;
    }
    .user:hover{
      background: #c2c2c2;
    }
    `
  ]
})
export class TeamsDetailComponent {
  @Input() team: Team;
  newTeam: Team;
  @Output() save: EventEmitter<Team> = new EventEmitter();
  visible: boolean;
  canI: boolean;
  role: string;
  member: string;
  userID: string;
  alreadyUsed = new Map();
  filteredUsers: Array<string>;
  mapOfUsers = new Map();
  users: User[];

  public selectedTeam: Team;

  public onSubmit(): void {
    this.save.emit(this.newTeam);
  }

  constructor(private teamService: CreateTeamService, private userService: AllUsersService) {
    this.newTeam = {id: null, teamName: '', members: {TEAM: [], SCRUM_MASTER: [], PRODUCT_OWNER: []}};
    this.role = 'Developer';
    this.visible = false;
    this.userService.get().subscribe((ret) => {
      this.users = ret;
    });
  }

  intoString() {
    if (this.mapOfUsers.size === 0) {
      this.users.forEach((value) => {
        if (value.email != null) {
          this.mapOfUsers.set(value.id, value.email);
        }
      });
      console.log(this.mapOfUsers);
    }
  }

  fillInput(user: string) {
    this.member = user;
  }

  filter() {
    this.filteredUsers = [];
    this.mapOfUsers.forEach((value: string, key: string) => {
      if (value.startsWith(this.member)) {
        this.filteredUsers.push(value);
      }
    });
  }

  changeVisibility() {
    this.visible = !this.visible;
    this.intoString();
  }

  deleteMe(member: string, role: string) {
    this.alreadyUsed.forEach((value: string, key: string) => {
      if (value === member){
        this.alreadyUsed.delete(key);
      }
    });

    if (role === 'Developer') {
      const index = this.newTeam.members.TEAM.indexOf(member, 0);
      if (index > -1) {
        this.newTeam.members.TEAM.splice(index, 1);
      }
    } else if (role === 'Product Owner') {
      const index = this.newTeam.members.PRODUCT_OWNER.indexOf(member, 0);
      if (index > -1) {
        this.newTeam.members.PRODUCT_OWNER.splice(index, 1);
      }
    }
  }

  addMember() {
    this.canI = false;

    this.mapOfUsers.forEach((value: string, key: string) => {
      if (value === this.member) {
        if (this.alreadyUsed.get(value) === undefined){
          this.canI = true;
          this.userID = key;
        }
      }
    });

    if (this.canI) {

      if (this.role === 'Developer') {
        this.newTeam.members.TEAM.push(this.userID);
        this.alreadyUsed.set(this.member, 'Developer');
      } else if (this.role === 'Product Owner') {
        this.newTeam.members.PRODUCT_OWNER.push(this.userID);
        this.alreadyUsed.set(this.member, 'Product Owner');
      }

      this.member = '';

      console.log(this.newTeam);
    }
  }
}
