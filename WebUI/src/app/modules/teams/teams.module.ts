import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { GridModule } from '@syncfusion/ej2-angular-grids';
import { PageService, SortService, FilterService, GroupService } from '@syncfusion/ej2-angular-grids';
import { TeamsComponent } from './container/teams-container.component';
import { TeamsDetailComponent } from './components/detail/teams-detail.component';
import { TeamsTableComponent } from './components/table/teams-table.component';



@NgModule({
  declarations: [
    TeamsComponent,
    TeamsDetailComponent,
    TeamsTableComponent
  ],
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    GridModule
  ],
  exports: [
    FormsModule
  ],
  providers: [
    PageService,
    SortService,
    FilterService,
    GroupService]
})
export class TeamsModule { }
