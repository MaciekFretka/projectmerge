import { Component, OnInit, ViewChild, Input, Output, EventEmitter } from '@angular/core';
import { GridComponent } from '@syncfusion/ej2-angular-grids';
import { Meeting } from 'src/app/models/meeting';

@Component({
  selector: 'app-meetings-table-component',
  templateUrl: './meetings-table.component.html',
  styles: [`
    label { font-size: 14px; height: 14px; margin-left: 6px;}
    .all-screen { width: 100%; height: 100%; }
  `]
})
export class MeetingsTableComponent{
  @Input() meetings: Meeting[];
  @Output() selected: EventEmitter<Meeting> = new EventEmitter();
  @ViewChild('grid', {static: true}) public grid: GridComponent;

  public select(): void {
    const select: Meeting[] = this.grid.getSelectedRecords() as Meeting[];
    this.selected.emit(select[0]);
  }
}
