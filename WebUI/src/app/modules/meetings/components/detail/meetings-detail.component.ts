import { Component, OnInit, ViewChild, Input, Output, EventEmitter } from '@angular/core';
import { NgForm } from '@angular/forms';
import { Route } from '@angular/router';
import { User } from 'src/app/models/user';
import { LoginService } from 'src/app/services/login.service';
import { Meeting } from 'src/app/models/meeting';

@Component({
  selector: 'app-meetings-details-component',
  templateUrl: './meetings-detail.component.html',
  styles: [`
    label { font-size: 14px; height: 14px; margin-left: 6px;}
    .all-screen { width: 100%; height: 100%; }
  `]
})
export class MeetingsDetailComponent{
  @Input() meeting: Meeting;
  @Output() save: EventEmitter<Meeting> = new EventEmitter();

  public selectedMeeting: Meeting;

  public onSubmit(): void{
    this.save.emit(this.meeting);
  }
}
