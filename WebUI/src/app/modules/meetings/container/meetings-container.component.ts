import { Component, OnInit, ViewChild, Input, OnChanges, SimpleChange, SimpleChanges } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Meeting } from 'src/app/models/meeting';
import { MeetingService } from 'src/app/services/meeting.service';

@Component({
  selector: 'app-meetings-container',
  templateUrl: './meetings-container.component.html',
  styles: [`
    .all-screen { width: 100%; height: 100%; }
    .margin{margin: 25px;}
  `]
})
export class MeetingsComponent implements OnInit, OnChanges{
  public allMeetings: Meeting[];
  public selectedMeeting: Meeting;
  public meetings: Meeting[];
  @Input() private teamId: string;
  @Input() private sprintId: string;

  constructor(private meetingService: MeetingService, private route: ActivatedRoute){
    this.meetings = [] as Meeting[];
    this.selectedMeeting = { sprintId: this.sprintId } as Meeting;
  }

  ngOnInit() {
    if (!this.teamId) {
      this.teamId = this.route.snapshot.paramMap.get('id');
    }
    this.meetingService.load(this.teamId).subscribe((result: Meeting[]) => {
     this.allMeetings = result;
    });
  }

  ngOnChanges(changes: SimpleChanges) {
    if (changes.sprintId || changes.teamId) {
      console.log(this.sprintId, this.teamId);
      this.meetings = this.allMeetings.filter((m: Meeting) => m.sprintId === this.sprintId);
      this.selectedMeeting = { sprintId: this.sprintId } as Meeting;
    }
  }

  add() {
    this.selectedMeeting = { sprintId: this.sprintId } as Meeting;
  }

  async del() {
    await this.meetingService.remove(this.teamId, this.selectedMeeting).toPromise();
    await this.refresh();
  }

  selected(meeting: Meeting): void{
     this.selectedMeeting = meeting;
  }

  async save(meeting: Meeting) {
    this.selectedMeeting = { sprintId: this.sprintId } as Meeting;
    if (this.meetings !== null && this.meetings.filter(x => x.id === meeting.id).length !== 0) {
      await this.meetingService.update(this.teamId, meeting).toPromise();
    } else {
      await this.meetingService.create(this.teamId, meeting).toPromise();
    }
    await this.refresh();
  }

  async refresh() {
    const res = this.meetingService.load(this.teamId);
    await res.toPromise();
    res.subscribe((result: Meeting[]) => {
    this.allMeetings = result;
    });
    this.meetings = this.allMeetings.filter((m: Meeting) => m.sprintId === this.sprintId);
  }
}
