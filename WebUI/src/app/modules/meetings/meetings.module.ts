import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { GridModule } from '@syncfusion/ej2-angular-grids';
import { PageService, SortService, FilterService, GroupService } from '@syncfusion/ej2-angular-grids';
import { MeetingsComponent } from './container/meetings-container.component';
import { MeetingsDetailComponent } from './components/detail/meetings-detail.component';
import { MeetingsTableComponent } from './components/table/meetings-table.component';



@NgModule({
  declarations: [
    MeetingsComponent,
    MeetingsDetailComponent,
    MeetingsTableComponent
  ],
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    GridModule
  ],
  exports: [
    FormsModule,
    MeetingsComponent,
    MeetingsDetailComponent,
    MeetingsTableComponent
  ],
  providers: [
    PageService,
    SortService,
    FilterService,
    GroupService]
})
export class MeetingsModule { }
