import { Component, OnInit, ViewChild } from '@angular/core';
import { NgForm } from '@angular/forms';
import { Route } from '@angular/router';
import { CreateTeamService } from 'src/app/services/team.service';
import { Team } from 'src/app/models/team';
import { Members } from 'src/app/models/members';

@Component({
  selector: 'app-create-team',
  templateUrl: './create-team.component.html',
<<<<<<< HEAD
  styles: [
    `
      label {
        font-size: 14px;
        height: 14px;
        margin-left: 6px;
      }
      .all-screen {
        width: 100%;
        height: 100%;
      }
    `,
  ],
=======
  styles: [`
    label { font-size: 14px; height: 14px; margin-left: 6px;}
    .all-screen { width: 100%; height: 100%; }
  `]
>>>>>>> login
})
export class CreateTeamComponent implements OnInit {
  team: Team;
  member: string;
  role: string;
  listOfMembers = new Map();

  constructor(private teamService: CreateTeamService) {
<<<<<<< HEAD
    this.team = {
      id: null,
      teamName: '',
      members: { SCRUM_MASTER: [], PRODUCT_OWNER: [], TEAM: [] },
    };
=======
    this.team = { id: '', teamName: '', members: { SCRUM_MASTER: [], PRODUCT_OWNER: [], TEAM: [] } };
>>>>>>> login
  }

  onSubmit() {
    this.teamService.createTeam(this.team);
    console.log(this.team.teamName);
  }

  ngOnInit() {
    // TODO
  }

  deleteMe(member: string, role: string) {
    this.listOfMembers.delete(member);
    if (role === 'Developer') {
      const index = this.team.members.TEAM.indexOf(member, 0);
      if (index > -1) {
        this.team.members.TEAM.splice(index, 1);
      }
<<<<<<< HEAD
    } else if (role === 'Product Owner') {
=======
    }
    else if (role === 'Product Owner') {
>>>>>>> login
      const index = this.team.members.PRODUCT_OWNER.indexOf(member, 0);
      if (index > -1) {
        this.team.members.PRODUCT_OWNER.splice(index, 1);
      }
    }
  }

  addMember() {
<<<<<<< HEAD
    if (this.role === 'Developer') {
      this.team.members.TEAM.push(this.member);
    } else if (this.role === 'Product Owner') {
      this.team.members.PRODUCT_OWNER.push(this.member);
    }
    console.log(this.role);

=======

    if (this.role === 'Developer') {
      this.team.members.TEAM.push(this.member);
    }
    else if (this.role === 'Product Owner') {
      this.team.members.PRODUCT_OWNER.push(this.member);
    }
>>>>>>> login
    this.listOfMembers.set(this.member, this.role);
    this.member = '';

    console.log(this.team);
    // this.teamService.addUserToTeam();
  }
}
