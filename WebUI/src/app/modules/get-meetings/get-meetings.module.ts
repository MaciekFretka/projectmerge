import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { GetMeetingsComponent } from './get-meetings.component';


@NgModule({
  declarations: [GetMeetingsComponent],
  imports: [
    CommonModule
  ]
})
export class GetMeetingsModule { }
