import { Component, OnInit } from '@angular/core';
import { GetMeetingsService } from '../../services/get-meetings.service';
import { Meeting } from 'src/app/models/meeting';

@Component({
  selector: 'app-get-meetings',
  templateUrl: './get-meetings.component.html',
  styleUrls: ['./get-meetings.component.css']
})
export class GetMeetingsComponent implements OnInit {
  public test: Meeting[];

  constructor(private getMeetingsSevice : GetMeetingsService) { }

  ngOnInit(): void {
    this.getMeetingsSevice.load().subscribe((ret) => {
      this.test = ret;
    });
  }

}
