import { Component, OnInit, ViewChild } from '@angular/core';
import { NgForm } from '@angular/forms';
import { Route } from '@angular/router';
import { CreateTeamService } from 'src/app/services/team.service';
import { Team } from 'src/app/models/team';
import { Members } from 'src/app/models/members';
import { stringify } from 'querystring';

@Component({
  selector: 'app-edit-team',
  templateUrl: './edit-team.component.html',
  styles: [`
    label { font-size: 14px; height: 14px; margin-left: 6px;}
    .all-screen { width: 100%; height: 100%; }
  `]
})
export class EditTeamComponent implements OnInit {
  team: Team = { id: '5ea823a5286a3b0992732a21', teamName: 'Rozrabiaki',
  members: { PRODUCT_OWNER: ['Owner'], SCRUM_MASTER: ['Master'], TEAM: ['some1'] } };
  listOfMembers = new Map<string, string>();
  member: string;
  role: string;
  smt: any;

  constructor(private teamService: CreateTeamService) {
  }

  ngOnInit(): void {
    this.team.members.PRODUCT_OWNER.forEach((value: string) => {
      this.listOfMembers.set(value, 'Product Owner');
    });
    this.team.members.TEAM.forEach((value: string) => {
      this.listOfMembers.set(value, 'Developer');
    });
  }

  deleteMe(member: string, role: string) {
    this.listOfMembers.delete(member);
    if (role === 'Developer') {
      const index = this.team.members.TEAM.indexOf(member, 0);
      if (index > -1) {
        this.team.members.TEAM.splice(index, 1);
      }
    }
    else if (role === 'Product Owner') {
      const index = this.team.members.PRODUCT_OWNER.indexOf(member, 0);
      if (index > -1) {
        this.team.members.PRODUCT_OWNER.splice(index, 1);
      }
    }
  }

  submit(){
    console.log(this.team);
    this.teamService.editTeam(this.team);
  }

  addMember() {

    if (this.role === 'Developer') {
      this.team.members.TEAM.push(this.member);
    }
    else if (this.role === 'Product Owner') {
      this.team.members.PRODUCT_OWNER.push(this.member);
    }
    console.log(this.role);
    this.listOfMembers.set(this.member, this.role);
    this.member = '';

    console.log(this.team);
    // this.teamService.addUserToTeam();
  }

  deleteTeam(){
    this.teamService.deleteTeam(this.team.id);
  }
}
