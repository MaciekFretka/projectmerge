import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Meeting } from '../models/meeting';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class GetMeetingsService {
  
  private metings: Observable<Meeting[]>;
  constructor(private http: HttpClient) {}
  url ='http://localhost:8081/users/5eba8603a80f214399a749b4/meetings'
  public load(): Observable<Meeting[]>{
    this.metings = this.http.get<Meeting[]>(this.url);
    return this.metings;
  }
  public get(): Observable<Meeting[]>{
    return this.metings;
  }
}
