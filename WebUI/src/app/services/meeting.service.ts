import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable, of } from 'rxjs';
import { Meeting } from '../models/meeting';
import { catchError } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class MeetingService {
    private meetings: Observable<Meeting[]>;

    constructor(private http: HttpClient) { }

    public load(teamId: string): Observable<Meeting[]> {
        this.meetings = this.http.get<Meeting[]>(`http://localhost:8081/teams/${teamId}/meetings`);
        return this.meetings;
    }

    public update(teamId: string, meeting: Meeting): Observable<Meeting> {
        return this.http.put<Meeting>(`http://localhost:8081/teams/${teamId}/meetings/${meeting.id}`, meeting);
    }

    public create(teamId: string, meeting: Meeting): Observable<Meeting> {
        return this.http.post<Meeting>(`http://localhost:8081/teams/${teamId}/meetings`, meeting).pipe(
            catchError(err => {
                console.log(err);
                return of(err);
        }));
    }

    public remove(teamId: string, meeting: Meeting): Observable<Meeting> {
        return this.http.delete<Meeting>(`http://localhost:8081/teams/${teamId}/meetings/${meeting.id}`);
    }

    public get(): Observable<Meeting[]> {
        return this.meetings;
    }
}
