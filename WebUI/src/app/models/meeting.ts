export interface Meeting{
    id: number;
    sprintId: string;
    meetingName: string;
    members: object[];
    startDateTime: Date | string;
    endDateTime: Date | string;
}
