package com.scruum.mastersofscruum.service;

import com.scruum.mastersofscruum.models.User;
import com.scruum.mastersofscruum.repositories.UserRepository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
public class UserDetailsServiceImpl implements UserDetailsService {
    @Autowired
    UserRepository userRepository;

    @Override
    @Transactional
    public UserDetails loadUserByUsername(String email) throws UsernameNotFoundException {
        if(!userRepository.existsByEmail(email)){
            throw new UsernameNotFoundException("User Not Found with username: " + email);
        }
        User user = userRepository.findByEmail(email);

        return UserDetailsImpl.build(user);
    }

}
