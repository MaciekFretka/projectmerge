package com.scruum.mastersofscruum.service.Impl;

import com.scruum.mastersofscruum.model.Sprint;
import com.scruum.mastersofscruum.model.Team;
import com.scruum.mastersofscruum.repository.SprintRepository;
import com.scruum.mastersofscruum.repository.TeamRepository;
import com.scruum.mastersofscruum.service.SprintService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class SprintServiceImpl implements SprintService {

    private final SprintRepository sprintRepository;
    private final TeamRepository teamRepository;

    @Autowired
    public SprintServiceImpl(SprintRepository sprintRepository, TeamRepository teamRepository) {
        this.sprintRepository = sprintRepository;
        this.teamRepository = teamRepository;
    }

    public SprintRepository sprintRepository() {
        return this.sprintRepository;
    }

    @Override
    public boolean deleteById(String teamId, String sprintId) {
        Optional<Team> optionalTeam = teamRepository.findById(teamId);
        if (optionalTeam.isPresent()) {
            Team team = optionalTeam.get();
            if (sprintRepository.existsById(sprintId) && team.getSprintList().contains(sprintId)) { // add check team sprint list
                team.getSprintList().remove(sprintId);
                team = teamRepository.save(team);
                sprintRepository.deleteById(sprintId);
                if (!teamRepository.existsById(sprintId)) {
                    return true;
                }
            }
        }
        return false;
    }

    @Override
    public List<Sprint> findByTeamId(String teamId) {
        Optional<Team> optionalTeam = teamRepository.findById(teamId);
        if (optionalTeam.isPresent()) {
            Team team = optionalTeam.get();
            List<Sprint> list = new ArrayList<>();
            for (String id : team.getSprintList()) {
                Optional<Sprint> optionalSprint = sprintRepository.findById(id);
                if(optionalSprint.isPresent()) {
                    Sprint sprint = optionalSprint.get();
                    list.add(sprint);
                }
            }
            return list;
        }
        return new ArrayList<>();
    }

    @Override
    public Sprint save(String teamId, Sprint sprint) {
        Optional<Team> optionalTeam = teamRepository.findById(teamId);
        if (teamRepository.existsById(teamId)) {
            sprint.setId(null);
            sprint = sprintRepository.save(sprint);
            Team team = optionalTeam.get();
            team.getSprintList().add(sprint.getId());
            teamRepository.save(team);
            return sprint;
        }
        return null;
    }

}
