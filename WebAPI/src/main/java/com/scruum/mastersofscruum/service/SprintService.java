package com.scruum.mastersofscruum.service;

import com.scruum.mastersofscruum.model.Sprint;
import com.scruum.mastersofscruum.repository.SprintRepository;

import java.util.List;


public interface SprintService {

    public SprintRepository sprintRepository();

    public boolean deleteById(String teamId, String sprintId);

    public List<Sprint> findByTeamId(String teamId);

    public Sprint save(String teamId, Sprint sprint);

}
