package com.scruum.mastersofscruum.service;

import com.scruum.mastersofscruum.model.Meeting;
import com.scruum.mastersofscruum.model.User;

import java.util.List;

public interface MeetingService {

    Meeting save(String teamId, Meeting meeting);

    Meeting update(String teamId, Meeting meeting);

    List<Meeting> findAll(String teamId);

    boolean deleteByMeetingName(String teamId, Integer meetingId);

    Meeting findById(String teamId, Integer meetingId);

    List<User> findMembersByMeetingId(String teamId, Integer meetingId);

    List<Meeting> getMeetings(String userId);

    List<Meeting> findMeetingsByUserId(String userId);
}
