package com.scruum.mastersofscruum.service.Impl;

import com.scruum.mastersofscruum.model.Meeting;
import com.scruum.mastersofscruum.model.Role;
import com.scruum.mastersofscruum.model.Team;
import com.scruum.mastersofscruum.model.User;
import com.scruum.mastersofscruum.repository.UserRepository;
import com.scruum.mastersofscruum.service.MeetingService;
import com.scruum.mastersofscruum.service.TeamService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.aggregation.Aggregation;
import org.springframework.data.mongodb.core.aggregation.AggregationResults;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.server.ResponseStatusException;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

import static org.springframework.data.mongodb.core.aggregation.Aggregation.*;

@Service
public class MeetingServiceImpl implements MeetingService {

    private final TeamService teamService;
    private final UserRepository userRepository;
    private final MongoTemplate mongoTemplate;

    @Autowired
    public MeetingServiceImpl(TeamService teamService, UserRepository userRepository, MongoTemplate mongoTemplate) {
        this.teamService = teamService;
        this.userRepository = userRepository;
        this.mongoTemplate = mongoTemplate;
    }

    private Team getTeam(String teamId) {
        Optional<Team> optionalTeam = teamService.teamRepository().findById(teamId);
        if (optionalTeam.isEmpty()) {
            return null;
        }
        return optionalTeam.get();
    }

    @Override
    public Meeting save(String teamId, Meeting meeting) {
        Team team = getTeam(teamId);
        if (meeting == null) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST);
        }
        if (team.getMeetingList() == null || team.getMeetingList().size() == 0) {
            team.setMeetingList(new ArrayList<>());
            meeting.setId(1);
        } else {
            meeting.setId(team.getMeetingList().get(team.getMeetingList().size() - 1).getId() + 1);
        }
        team.getMeetingList().add(meeting);
        team = teamService.teamRepository().save(team);
        return meeting;
    }

    @Override
    public Meeting update(String teamId, Meeting meeting) {
        Team team = getTeam(teamId);
        if (meeting == null || team.getMeetingList() == null || team.getMeetingList().size() == 0) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST);
        }
        for (int i = 0; i < team.getMeetingList().size(); i++) {
            if (team.getMeetingList().get(i).getId() == meeting.getId()) {
                team.getMeetingList().set(i, meeting);
            }
        }
        team = teamService.teamRepository().save(team);
        return meeting;
    }

    @Override
    public List<Meeting> findAll(String teamId) {
        Optional<Team> optionalTeam = teamService.teamRepository().findById(teamId);
        if (optionalTeam.isEmpty()) {
            return null;
        }
        Team team = optionalTeam.get();
        if (team.getMeetingList() == null) {
            return new ArrayList<>();
        }
        return team.getMeetingList();
    }

    @Override
    public boolean deleteByMeetingName(String teamId, Integer meetingId) {
        Team team = getTeam(teamId);
        if (meetingId == null) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST);
        }
        for (int i = 0; i < team.getMeetingList().size(); i++) {
            if (team.getMeetingList().get(i).getId() == meetingId) {
                team.getMeetingList().remove(i);
            }
        }
        team = teamService.teamRepository().save(team);
        return true;
    }

    @Override
    public Meeting findById(String teamId, Integer meetingId) {
        Team team = getTeam(teamId);
        if (meetingId == null) {
            return null;
        }
        for (int i = 0; i < team.getMeetingList().size(); i++) {
            if (team.getMeetingList().get(i).getId() == meetingId) {
                return team.getMeetingList().get(i);
            }
        }
        return null;
    }

    @Override
    public List<User> findMembersByMeetingId(String teamId, Integer meetingId) {
        Team team = getTeam(teamId);
        if (meetingId == null) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND);
        }
        Meeting meeting = team.getMeetingList().get(meetingId);
        List<String> usersId = new ArrayList<>();
        for (Role role : meeting.getMembers()) {
            usersId.addAll(team.getMembers().get(role));
        }
        return StreamSupport.stream(userRepository.findAllById(usersId).spliterator(), false)
                .collect(Collectors.toList());
    }

    @Override
    public List<Meeting> findMeetingsByUserId(String userId) {
        List<Meeting> meetingList = new ArrayList<>();
        for (Role role : Role.values()) {
            Aggregation aggregation = newAggregation(
                    match((Criteria.where("members." + role).is(userId))),
                    unwind("$meetingList"),
                    replaceRoot().withValueOf("$meetingList"),
                    match(Criteria.where("members").is(role))
            );
            AggregationResults<Meeting> results = mongoTemplate.aggregate(aggregation, "team", Meeting.class);
            List tmp = results.getMappedResults();
            if (tmp != null && tmp.size() > 0) {
                meetingList.addAll(tmp);
            }
        }
        return meetingList;
    }

    @Deprecated
    public List<Meeting> getMeetings(String userId) {
        List<Team> teams = teamService.teamRepository().findTeamByMember(userId);
        List<Meeting> meetings = new ArrayList<>();
        for (Team team : teams) {
            for (Role role : team.getMembers().keySet()) {
                for (int i2 = 0; i2 < team.getMembers().get(role).size(); i2++) {
                    if (userId.equals(team.getMembers().get(role).get(i2))) {
                        for (int i3 = 0; i3 < team.getMeetingList().size(); i3++) {
                            for (int i4 = 0; i4 < team.getMeetingList().get(i3).getMembers().size(); i4++) {
                                if (team.getMeetingList().get(i3).getMembers().get(i4).equals(role) && team.getMeetingList().get(i3) != null) {
                                    meetings.add(team.getMeetingList().get(i3));
                                }
                            }
                        }
                    }
                }
            }
        }
        return meetings;
    }
}

