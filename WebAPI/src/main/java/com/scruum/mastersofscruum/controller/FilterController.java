package com.scruum.mastersofscruum.controller;

import com.scruum.mastersofscruum.model.Meeting;
import com.scruum.mastersofscruum.model.Team;
import com.scruum.mastersofscruum.service.MeetingService;
import com.scruum.mastersofscruum.service.TeamService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@CrossOrigin(origins = "*", allowedHeaders = "*")
@RestController
public class FilterController {
    @Autowired
    TeamService teamService;
    @Autowired
    MeetingService meetingService;

    /**
     * Get user's teams
     * @param userId - id usera
     * @return - list of user's team
     */
    @GetMapping(value = "users/{userId}/teams", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<List<Team>> getUserTeams(@PathVariable String userId){
        return new ResponseEntity<List<Team>>(teamService.teamRepository().findTeamByMember(userId), HttpStatus.OK);
    }

    @GetMapping(value = "users/{userId}/meetings", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<List<Meeting>> getMeetingByUserId(@PathVariable String userId){
        return new ResponseEntity<List<Meeting>>(meetingService.findMeetingsByUserId(userId), HttpStatus.OK);
    }
}
