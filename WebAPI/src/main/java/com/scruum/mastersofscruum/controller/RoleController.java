package com.scruum.mastersofscruum.controller;

import com.scruum.mastersofscruum.models.ERole;
import com.scruum.mastersofscruum.payload.JwtResponse;
import com.scruum.mastersofscruum.payload.LoginRequest;
import com.scruum.mastersofscruum.repositories.RoleRepository;
import com.scruum.mastersofscruum.repositories.UserRepository;
import com.scruum.mastersofscruum.service.UserDetailsImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;


@RestController
@RequestMapping("/api/apiRoles")
public class RoleController {

        @Autowired
        UserRepository userRepository;

        @Autowired
        RoleRepository roleRepository;

    @GetMapping(value = "/roles", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<List<ERole>> getRoleUsers() {
        List<ERole> listRoles = new ArrayList<>();
        listRoles.add(ERole.ROLE_USER);
        listRoles.add(ERole.ROLE_ADMIN);
        listRoles.add(ERole.ROLE_MODERATOR);
        return new ResponseEntity<List<ERole>>(listRoles, HttpStatus.OK);
    }

}
