package com.scruum.mastersofscruum.controller;

import com.scruum.mastersofscruum.model.User;
import com.scruum.mastersofscruum.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@CrossOrigin(origins = "*", allowedHeaders = "*")
@RestController
public class UserController {

    @Autowired
    UserRepository userRepository;

    @RequestMapping(value = "/addNewUser", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<User> restMethod(String firstName, String lastName, String role, String email, String password) {
        User user = new User(firstName, lastName, email,password);
        user = userRepository.save(user);

        return new ResponseEntity<User>(user, HttpStatus.OK);
    }

    @RequestMapping(value = "/getAllUsers", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<List<User>> getAllUsers() {
        List<User> users = userRepository.findAll();
        if(users == null){
            return new ResponseEntity("Empty DB or bad connection", HttpStatus.NOT_FOUND);
        }
        return new ResponseEntity<List<User>>(users, HttpStatus.OK);
    }



    @GetMapping(value = "users/{userID}", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<User> getUser(@PathVariable String userID){
        Optional<User> optionalUser = userRepository.findById(userID);
        if (optionalUser.isEmpty()) {
            return new ResponseEntity(null, HttpStatus.NO_CONTENT);
        }
        User user = optionalUser.get();
        return new ResponseEntity<User>(user, HttpStatus.OK);
    }

    @GetMapping(value = "users", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<List<User>> getUsers(@RequestParam(required = false) Integer startIndex, @RequestParam(required = false) Integer startIndexOffset, @RequestParam(required = false) String email, @RequestParam(required = false) String firstName, @RequestParam(required = false)String lastName) {
        List<User> teams = new ArrayList<User>();
        if (startIndex == null) {
            startIndex = 0;
        }
        if (startIndexOffset == null || startIndexOffset == 0) {
            startIndexOffset = (int) userRepository.count();
        }
        if (email != null && !email.equals("")) {
            teams.add(userRepository.findByEmail(email));
        }
        if ((lastName != null && !lastName.equals(""))) {
            if(firstName != null && !firstName.equals("")) {
                teams.addAll(userRepository.findByLastNameAndFirstName(lastName, firstName).subList(startIndex, startIndexOffset));
            }else {
                teams.addAll(userRepository.findByLastName(lastName).subList(startIndex, startIndexOffset));
            }
        }else {
            if(firstName != null && !firstName.equals("")) {
                teams.addAll(userRepository.findByFirstName(firstName).subList(startIndex, startIndexOffset));
            }
        }
        if ((lastName == null || lastName.equals("") && (email == null || email.equals("")) && (firstName == null || firstName.equals("")))) {
            teams.addAll(userRepository.findAll().subList(startIndex, startIndexOffset));
        }
        if (teams.size() == 0) {
            return new ResponseEntity(null, HttpStatus.NO_CONTENT);
        }
        return new ResponseEntity<List<User>>(teams, HttpStatus.OK);
    }

    @PutMapping(value = "users/{userID}", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<User> updateUser(@PathVariable String userID, @RequestBody User user) {
        Optional<User> optionalTeam = userRepository.findById(userID);
        if (optionalTeam.isEmpty()) {
            return new ResponseEntity<User>(user, HttpStatus.NO_CONTENT);
        }
        user.setId(userID);
        user = userRepository.save(user);
        return new ResponseEntity<User>(user, HttpStatus.OK);
    }

    @DeleteMapping(value = "users/{userID}", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<User> deleteUser(@PathVariable String userID) {
        if (userRepository.existsById(userID)) {
            userRepository.deleteById(userID);
            return new ResponseEntity(null, HttpStatus.OK);
        }
        return new ResponseEntity(null, HttpStatus.NO_CONTENT);
    }
}
