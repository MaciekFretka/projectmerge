package com.scruum.mastersofscruum.controller;


<<<<<<< HEAD
import com.scruum.mastersofscruum.model.User;
import com.scruum.mastersofscruum.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

=======
import com.scruum.mastersofscruum.models.User;
import com.scruum.mastersofscruum.repositories.UserRepository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.*;




>>>>>>> login
@CrossOrigin(origins = "*", allowedHeaders = "*")
@RestController
public class LoginController {

<<<<<<< HEAD
    @Autowired
    UserRepository userRepository;
=======

    private final  UserRepository userRepository;
    private final BCryptPasswordEncoder passwordEncoder;

    @Autowired
    public LoginController(UserRepository userRepository,BCryptPasswordEncoder passwordEncoder) {
        this.userRepository = userRepository;
        this.passwordEncoder = passwordEncoder;
    }

>>>>>>> login

    /**LOGIN**/
    @RequestMapping(value = "/loginUser", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<User> loginUser(String email, String password) {
        User user = userRepository.findByEmailAndPassword(email,password);
        if(user == null){
            return new ResponseEntity("User not found", HttpStatus.UNAUTHORIZED);
        }
        return new ResponseEntity<User>(user, HttpStatus.OK);
    }

    /**REGISTRATION**/
<<<<<<< HEAD
    @PostMapping(value = "/RegistrationUser", produces = MediaType.APPLICATION_JSON_VALUE)
=======
    @RequestMapping(value = "/RegistrationUser", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
>>>>>>> login
    public ResponseEntity<User> registrationUser(@RequestBody User user) {
        if(userRepository.findByEmail(user.getEmail()) != null){
            return new ResponseEntity("User exists", HttpStatus.FOUND);
        }
        User newUser = new User();
        user.setId(newUser.getId());
<<<<<<< HEAD
=======
        //user.setPassword(passwordEncoder.encode(user.getPassword()));
>>>>>>> login
        user = userRepository.save(user);
        return new ResponseEntity<User>(user, HttpStatus.OK);
    }
}
