package com.scruum.mastersofscruum.controller;

import com.scruum.mastersofscruum.model.Meeting;
import com.scruum.mastersofscruum.model.User;
import com.scruum.mastersofscruum.service.MeetingService;
import com.scruum.mastersofscruum.service.TeamService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.ArrayList;
import java.util.List;

@CrossOrigin(origins = "*", allowedHeaders = "*")
@RestController
public class MeetingController {

    @Autowired
    MeetingService meetingService;
    @Autowired
    TeamService teamService;

    @PostMapping(value = "teams/{teamID}/meetings", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Meeting> createMeeting(@PathVariable String teamID, @RequestBody @Valid Meeting meeting) {
        meeting = meetingService.save(teamID, meeting);
        if (meeting != null) {
            return new ResponseEntity<Meeting>(meeting, HttpStatus.CREATED);
        }
        return new ResponseEntity(null, HttpStatus.INTERNAL_SERVER_ERROR);
    }

    @GetMapping(value = "teams/{teamID}/meetings", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<List<Meeting>> getMeetings(@PathVariable String teamID) {
        List<Meeting> meetings = new ArrayList<>();
        meetings.addAll(meetingService.findAll(teamID));
        return new ResponseEntity<List<Meeting>>(meetings, HttpStatus.OK);
    }

    @GetMapping(value = "teams/{teamID}/meetings/{meetingId}", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Meeting> getMeeting(@PathVariable String teamID, @PathVariable Integer meetingId) {
        Meeting meeting = meetingService.findById(teamID, meetingId);
        if (meeting == null) {
            return new ResponseEntity(null, HttpStatus.NOT_FOUND);
        }
        return new ResponseEntity<Meeting>(meeting, HttpStatus.OK);
    }

    @GetMapping(value = "teams/{teamID}/meetings/{meetingId}/members", produces = MediaType.APPLICATION_JSON_VALUE)
    private ResponseEntity<List<User>> getMeetingMembers(@PathVariable String teamID, @PathVariable Integer meetingId){
        List<User> members = meetingService.findMembersByMeetingId(teamID, meetingId);
        if (members == null) {
            return new ResponseEntity(null, HttpStatus.NOT_FOUND);
        }
        return new ResponseEntity<List<User>>(members, HttpStatus.OK);
    }

    @PutMapping(value = "teams/{teamID}/meetings/{meetingId}", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Meeting> updateMeeting(@PathVariable String teamID, @PathVariable Integer meetingId, @RequestBody @Valid Meeting meeting) {
        meeting.setId(meetingId);
        meeting = meetingService.update(teamID, meeting);
        if (meeting == null) {
            return new ResponseEntity(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
        return new ResponseEntity<Meeting>(meeting, HttpStatus.OK);
    }

    @DeleteMapping(value = "teams/{teamID}/meetings/{meetingId}", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<?> deleteMeeting(@PathVariable String teamID, @PathVariable Integer meetingId) {
        if (meetingService.deleteByMeetingName(teamID, meetingId)) {
            return new ResponseEntity(null, HttpStatus.OK);
        }
        return new ResponseEntity(null, HttpStatus.NOT_FOUND);
    }

}
