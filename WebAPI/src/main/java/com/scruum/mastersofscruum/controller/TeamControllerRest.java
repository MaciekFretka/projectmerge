package com.scruum.mastersofscruum.controller;

import com.scruum.mastersofscruum.models.Role;
import com.scruum.mastersofscruum.models.Team;
import com.scruum.mastersofscruum.repositories.TeamRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@CrossOrigin(origins = "*", allowedHeaders = "*")
@RestController
@RequestMapping("/teams")
public class TeamControllerRest {

    @Autowired
    TeamRepository teamRepository;
    @Autowired
    UserControllerRest controllerRest;
    private ApplicationContext applicationContext;

    @PostMapping(produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Team> createTeam(@RequestBody Team team) {
        //Team team = new Team(teamName, scrumMasterID);
        Optional<Team> optionalTeam = teamRepository.findById(team.getId());
        if (optionalTeam.isEmpty()) {
            team = teamRepository.save(team);
            return new ResponseEntity<Team>(team, HttpStatus.CREATED);
        }
        return new ResponseEntity<Team>(team, HttpStatus.CONFLICT);
    }

    @GetMapping(produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<List<Team>> getTeams(@RequestParam Integer startIndex, @RequestParam Integer startIndexOffset, @RequestParam String teamName) {
        List<Team> teams = new ArrayList<Team>();
        if (startIndex == null) {
            startIndex = 0;
        }
        if (startIndexOffset == null || startIndexOffset == 0) {
            startIndexOffset = (int) teamRepository.count();
        }
        if (teamName != null && !teamName.equals("")) {
            teams.addAll(teamRepository.findByTeamName(teamName).subList(startIndex, startIndexOffset));
        }
        if (teamName == null || teamName.equals("")) {
            teams.addAll(teamRepository.findAll().subList(startIndex, startIndexOffset));
        }
        if (teams.size() == 0) {
            return new ResponseEntity(null, HttpStatus.NO_CONTENT);
        }
        return new ResponseEntity<List<Team>>(teams, HttpStatus.OK);
    }

    @GetMapping(value = "/{teamID}", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Team> getTeam(@PathVariable String teamID) {
        Optional<Team> optionalTeam = teamRepository.findById(teamID);
        if (optionalTeam.isEmpty()) {
            return new ResponseEntity(null, HttpStatus.NO_CONTENT);
        }
        Team team = optionalTeam.get();
        return new ResponseEntity<Team>(team, HttpStatus.OK);
    }

    @PutMapping(value = "/{teamID}", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Team> updateTeam(@PathVariable String teamID, @RequestBody Team team) {
        Optional<Team> optionalTeam = teamRepository.findById(teamID);
        if (optionalTeam.isEmpty()) {
            return new ResponseEntity<Team>(team, HttpStatus.NO_CONTENT);
        }
        team.setId(teamID);
        team = teamRepository.save(team);
        return new ResponseEntity<Team>(team, HttpStatus.OK);
    }

    @DeleteMapping(value = "/{teamID}", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Team> deleteTeam(@PathVariable String teamID) {
        if (teamRepository.existsById(teamID)) {
            teamRepository.deleteById(teamID);
            return new ResponseEntity(null, HttpStatus.OK);
        }
        return new ResponseEntity(null, HttpStatus.NO_CONTENT);
    }


    //    @GetMapping(produces = MediaType.APPLICATION_JSON_VALUE)
//    public ResponseEntity<List<Team>> getAllTeams() {
//        List<Team> teams = teamRepository.findAll();
//        if (teams == null) {
//            return new ResponseEntity(null, HttpStatus.NO_CONTENT);
//        }
//        return new ResponseEntity<List<Team>>(teams, HttpStatus.OK);
//    }

    @RequestMapping(value = "/deleteUserFromTeam", method = RequestMethod.PUT, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Team> deleteUserFromTeam(String teamID, String userID) {
        Team team = teamRepository.deleteUserFromTeamByIdAndUserId(teamID, userID);
        if (team == null) {
            return new ResponseEntity("Empty DB or bad connection", HttpStatus.NOT_FOUND);
        }
        return new ResponseEntity<Team>(team, HttpStatus.OK);
    }

    @RequestMapping(value = "/changeUserRole", method = RequestMethod.PUT, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Team> changeUserRole(String teamID, String userID, int roleID) {
        Team team = teamRepository.updateUserRoleByIdAndUserId(teamID, userID, Role.values()[roleID]);
        if (team == null) {
            return new ResponseEntity("Empty DB or bad connection", HttpStatus.NOT_FOUND);
        }
        return new ResponseEntity<Team>(team, HttpStatus.OK);
    }

    @RequestMapping(value = "/addUserToTeam", method = RequestMethod.PUT, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Team> addUserToTeam(String teamID, String userID, int roleID) {
        Team team = teamRepository.updateAddToTeamById(teamID, userID, Role.values()[roleID]);
        if (team == null) {
            return new ResponseEntity("Empty DB or bad connection", HttpStatus.NOT_FOUND);
        }
        return new ResponseEntity<Team>(team, HttpStatus.OK);
    }
}
