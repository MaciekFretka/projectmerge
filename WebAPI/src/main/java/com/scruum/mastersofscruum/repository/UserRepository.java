package com.scruum.mastersofscruum.repository;

import com.scruum.mastersofscruum.model.User;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.Query;

import java.util.List;

public interface UserRepository extends MongoRepository<User, String>  {

    public List<User> findByLastNameAndFirstName(String lastName, String firstName);

    public List<User> findByLastName(String lastName);

    public List<User> findByFirstName(String firstName);

    public User findByEmailAndPassword(String email,String password);

    public User findByEmail(String email);


//    @Query("{ 'email' : { $regex: ?0 } }")
//    public User findUsersByEmailRegex(String regexEmail);

    @Query("{ 'email' : { $regex: ?0 } }")
    List<User> findUsersByRegexpEmail(String regexp);

}
