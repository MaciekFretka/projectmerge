package com.scruum.mastersofscruum.models;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import org.springframework.data.annotation.Id;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Team {
    @Id
    private String id;
    private String teamName;
    private Map<Role, List<String>> members;

    public Team(String teamName, String scrumMasterID) {
        this.teamName = teamName;
        this.members = new HashMap<>();
        members.put(Role.SCRUM_MASTER, new ArrayList<String>(1));
        members.get(Role.SCRUM_MASTER).add(0, scrumMasterID);
        members.put(Role.PRODUCT_OWNER, new ArrayList<String>(1));
        members.put(Role.TEAM, new ArrayList<String>());
    }

    public Team() {
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getTeamName() {
        return teamName;
    }

    public void setTeamName(String teamName) {
        this.teamName = teamName;
    }

    public Map<Role, List<String>> getMembers() {
        return members;
    }

    public void setMembers(Map<Role, List<String>> members) {
        this.members = members;
    }

    @Override
    public String toString() {
        Gson gson = new GsonBuilder().setPrettyPrinting().create();
        return gson.toJson(this);
    }

}
