package com.scruum.mastersofscruum.models;

public enum Role {
    SCRUM_MASTER,
    PRODUCT_OWNER,
    TEAM
}
