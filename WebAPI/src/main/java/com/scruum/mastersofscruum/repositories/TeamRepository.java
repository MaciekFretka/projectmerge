package com.scruum.mastersofscruum.repositories;

import com.scruum.mastersofscruum.models.Role;
import com.scruum.mastersofscruum.models.Team;
import com.scruum.mastersofscruum.models.User;
import org.springframework.data.mongodb.repository.MongoRepository;

import java.util.List;

public interface TeamRepository extends MongoRepository<Team, String>, TeamRepositoryCustom {

    public List<Team> findByTeamName(String teamName);

}
