package com.scruum.mastersofscruum.repositories;

import com.scruum.mastersofscruum.models.Role;
import com.scruum.mastersofscruum.models.Team;

public interface TeamRepositoryCustom {

    public Team updateAddToTeamById(String id, String userID,  Role role);

    public Team updateTeamById(String id, Team team);

    public Team deleteUserFromTeamByIdAndUserId(String id, String user);

    public Team updateUserRoleByIdAndUserId(String id, String userID,  Role role);

}
