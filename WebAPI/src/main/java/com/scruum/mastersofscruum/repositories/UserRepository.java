package com.scruum.mastersofscruum.repositories;

import com.scruum.mastersofscruum.models.User;
import org.springframework.data.mongodb.repository.MongoRepository;

import java.util.List;

public interface UserRepository extends MongoRepository<User, String>  {

    public List<User> findByLastNameAndFirstName(String lastName, String firstName);

    public List<User> findByLastName(String lastName);

    public List<User> findByFirstName(String firstName);

    public User findByEmailAndPassword(String email,String password);

    public User findByEmail(String email);

    Boolean existsByEmail(String email);



}
