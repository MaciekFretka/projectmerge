package com.scruum.mastersofscruum.repositories;

import com.scruum.mastersofscruum.models.Role;
import com.scruum.mastersofscruum.models.Team;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoOperations;
import org.springframework.util.Assert;

import java.util.List;


public class TeamRepositoryImpl implements TeamRepositoryCustom {
    MongoOperations mongoOperations;

    @Autowired
    public TeamRepositoryImpl(MongoOperations operations) {
        Assert.notNull(operations, "MongoOperations must not be null!");
        this.mongoOperations = operations;
    }


    @Override
    public Team updateAddToTeamById(String id, String userID, Role role) {
        Team team = mongoOperations.findById(id, Team.class);
        List<String> strings = team.getMembers().get(role);
        if (role == Role.SCRUM_MASTER || role == Role.PRODUCT_OWNER)
            strings.clear();
        strings.add(userID);
        team.getMembers().put(role, strings);
        mongoOperations.save(team);
        Team team1 = mongoOperations.findById(id, Team.class);
        return team1;
    }

    @Override
    public Team updateTeamById(String id, Team team) {
        Team tmp = mongoOperations.findById(id, Team.class);
        team.setId(tmp.getId());
        return mongoOperations.save(team);
    }

    @Override
    public Team deleteUserFromTeamByIdAndUserId(String id, String user) {
        Team team = mongoOperations.findById(id, Team.class);
        for (Role role : Role.values()) {
            if (team.getMembers().get(role).contains(user)) {
                team.getMembers().get(role).remove(user);
            }
        }
        mongoOperations.save(team);
        Team team1 = mongoOperations.findById(id, Team.class);
        return team1;
    }

    @Override
    public Team updateUserRoleByIdAndUserId(String id, String userID, Role role) {
        Team team = mongoOperations.findById(id, Team.class);
        for (Role roleIter : Role.values()) {
            if (team.getMembers().get(roleIter).contains(userID)) {
                team.getMembers().get(roleIter).remove(userID);
            }
        }
        team.getMembers().get(role).add(userID);
        mongoOperations.save(team);
        Team team1 = mongoOperations.findById(id, Team.class);
        return team1;
    }


}
